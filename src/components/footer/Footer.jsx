import React from 'react'
import './footer.css'
import {BsFacebook} from 'react-icons/bs'
import {GrInstagram} from 'react-icons/gr'
import {AiFillLinkedin} from 'react-icons/ai'

const Footer = () => {
  return (
    <footer>
      <a href="#" className='footer__logo'>WASEEM</a>

      <ul className='permalinks'>
        <li><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#experience">Experience</a></li>
        <li><a href="#services">Services</a></li>
        <li><a href="#portfolio">Portfolio</a></li>
        <li><a href="#testimonials">Testimonials</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>

      <div className="footer__socials">
        <a href="https://www.facebook.com/wasiem.alchmar/"><BsFacebook/></a>
        <a href="https://instagram.com/semo_12l?igshid=NTc4MTIwNjQ2YQ=="><GrInstagram/></a>
        <a href="https://www.linkedin.com/in/waseem-ashmar-3b9368171/"><AiFillLinkedin/></a>
      </div>

      <div className='footer__copyright'>
        <small>&copy; WASEEM Tutorials. All rights reserved</small>
      </div>
    </footer>
  )
}

export default Footer