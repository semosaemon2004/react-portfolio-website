import React from "react";
import "./services.css";
import { GiCheckMark } from "react-icons/gi";

const Services = () => {
  return (
    <section id="services">
      <h5>What I Offer</h5>
      <h2>Services</h2>

      <div className="container services__container">
        {/* <article className="service">
          <div className="service__head">
            <h3>UI/UX Design</h3>
          </div>
          <ul className="service__list">
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Add any sevices here</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Add any sevices here</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Add any sevices here</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Add any sevices here</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Add any sevices here</p>
            </li>
          </ul>
        </article> */}

        {/* END OF UI/UX */}

        <article className="service">
          <div className="service__head">
            <h3>Web Development</h3>
          </div>
          <ul className="service__list">
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Custom Website Development.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Responsive Web Design.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Front-end Development.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Back-end Development.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>E-commerce Development.</p>
            </li>
          </ul>
        </article>

        {/* END OF WEB DEVELOPMENT */}

        <article className="service">
          <div className="service__head">
            <h3>Mobile Development</h3>
          </div>
          <ul className="service__list">
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Native Mobile App Development.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Cross-platform Development.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>UI/UX Design.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>App Maintenance and Support.</p>
            </li>
            <li>
              <GiCheckMark className="service__list-icon" />
              <p>Mobile App Testing.</p>
            </li>
          </ul>
        </article>

        {/* END OF MOBILE DEVELOPMENT */}
      </div>
    </section>
  );
};

export default Services;
