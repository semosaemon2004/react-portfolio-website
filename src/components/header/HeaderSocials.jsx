import React from "react";
import {BsLinkedin} from 'react-icons/bs'
import {SiGitlab} from 'react-icons/si'
import {SiCanva} from 'react-icons/si'

const HeaderSocials = () => {
  return (
    <div className="header__socials">
      <a href="https://www.linkedin.com/in/waseem-ashmar-3b9368171/" target="_blank"><BsLinkedin/></a>
      <a href="https://gitlab.com/semosaemon2004" target="_blank"><SiGitlab/></a>
      <a href="https://canva.com" target="_blank"><SiCanva/></a>
    </div>
  );
};

export default HeaderSocials;
