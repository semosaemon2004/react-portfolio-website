import React from "react";
import "./portfolio.css";
import IMG1 from "../../assets/portfolio1.jpg";
import IMG2 from "../../assets/portfolio2.jpg";
import IMG3 from "../../assets/portfolio3.jpg";
import IMG4 from "../../assets/portfolio4.jpg";
import IMG5 from "../../assets/portfolio5.png";
import IMG6 from "../../assets/portfolio6.jpg";

// DO NOT USE THE IMAGES IN PRODUCTION

const data = [
  {
    id: 1,
    image: IMG1,
    title: "Ecommerce App",
    gitlap: "https://gitlab.com/semosaemon2004/flutter-ecommerce-app",
    demo: "https://www.youtube.com/shorts/dnRgkNPkrfw",
  },
  {
    id: 2,
    image: IMG2,
    title: "Medical App",
    gitlap:
      "https://gitlab.com/semosaemon2004/flutter_medical_healthcare_app_ui",
    demo: "https://www.youtube.com/shorts/SSftLikztxY",
  },
  {
    id: 3,
    image: IMG3,
    title: "Game Store Website",
    gitlap:
      "https://gitlab.com/semosaemon2004/react-games-store-website",
    demo: "https://waseem-game-store-website.netlify.app/",
  },
  {
    id: 4,
    image: IMG4,
    title: "BMI Calculate App",
    gitlap: "https://gitlab.com/semosaemon2004/flutter_bmi_calculate",
    demo: "https://www.youtube.com/shorts/6kU5O08nLZk",
  },
  {
    id: 5,
    image: IMG5,
    title: "To Do App",
    gitlap: "https://gitlab.com/semosaemon2004/flutter_to_do",
    demo: "https://www.youtube.com/shorts/wK5u3yQIl4Q",
  },
  {
    id: 6,
    image: IMG6,
    title: "Nike Shose App",
    gitlap: "https://gitlab.com/semosaemon2004/flutter_nike_shose_ui",
    demo: "https://www.youtube.com/shorts/oXBSKJUzPsw",
  },
];

const Portfolio = () => {
  return (
    <section id="portfolio">
      <h5>My Recent Work</h5>
      <h2>Portfolio</h2>

      <div className="container portfolio__container">
        {data.map(({ id, image, title, gitlap, demo }) => {
          return (
            <article key={id} className="portfolio__item">
              <div className="portfolio__item-image">
                <img src={image} alt={title} />
              </div>
              <h3>{title}</h3>
              <div className="portfolio__item-cta">
                <a href={gitlap} className="btn" target="_balnk">
                  GitLap
                </a>
                <a href={demo} className="btn btn-primary" target="_blank">
                  Live Demo
                </a>
              </div>
            </article>
          );
        })}
      </div>
    </section>
  );
};

export default Portfolio;
