import React from "react";
import "./testimonials.css";
import AVTR1 from "../../assets/avatar4.jpg";
import AVTR2 from "../../assets/avatar3.jpg";
import AVTR3 from "../../assets/avatar2.jpg";
import AVTR4 from "../../assets/avatar1.jpg";

// import Swiper core and required modules
import { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

const data = [
  {
    id: 1,
    image: AVTR1,
    name: "Nour",
    review:
      "Working with Waseem Ashmar has been an absolute pleasure. His expertise in web and mobile development is truly remarkable. Not only did he deliver exceptional results, but he also showed great attention to detail and a deep understanding of our project requirements. Waseem's ability to turn our vision into reality was impressive, and his strong problem-solving skills ensured that any challenges were overcome seamlessly. I highly recommend Waseem Ashmar for any web or mobile development needs; his professionalism, technical skills, and commitment to delivering outstanding solutions make him a valuable asset to any team or project.",
  },
  {
    id: 2,
    image: AVTR2,
    name: "Mohmmed",
    review:
      "Working with Waseem Ashmar has been an absolute pleasure. His expertise in web and mobile development is truly remarkable. Not only did he deliver exceptional results, but he also showed great attention to detail and a deep understanding of our project requirements. Waseem's ability to turn our vision into reality was impressive, and his strong problem-solving skills ensured that any challenges were overcome seamlessly. I highly recommend Waseem Ashmar for any web or mobile development needs; his professionalism, technical skills, and commitment to delivering outstanding solutions make him a valuable asset to any team or project.",
  },
  {
    id: 3,
    image: AVTR3,
    name: "Rami",
    review:
      "Working with Waseem Ashmar has been an absolute pleasure. His expertise in web and mobile development is truly remarkable. Not only did he deliver exceptional results, but he also showed great attention to detail and a deep understanding of our project requirements. Waseem's ability to turn our vision into reality was impressive, and his strong problem-solving skills ensured that any challenges were overcome seamlessly. I highly recommend Waseem Ashmar for any web or mobile development needs; his professionalism, technical skills, and commitment to delivering outstanding solutions make him a valuable asset to any team or project.",
  },
  {
    id: 4,
    image: AVTR4,
    name: "Sema",
    review:
      "Working with Waseem Ashmar has been an absolute pleasure. His expertise in web and mobile development is truly remarkable. Not only did he deliver exceptional results, but he also showed great attention to detail and a deep understanding of our project requirements. Waseem's ability to turn our vision into reality was impressive, and his strong problem-solving skills ensured that any challenges were overcome seamlessly. I highly recommend Waseem Ashmar for any web or mobile development needs; his professionalism, technical skills, and commitment to delivering outstanding solutions make him a valuable asset to any team or project.",
  },
];

const Testimonials = () => {
  return (
    <section id="testimonials">
      <h5>Review from clients</h5>
      <h2>Testimonials</h2>

      <Swiper
        className="container testimonials__container"
        // install Swiper modules
        modules={[Pagination]}
        spaceBetween={40}
        slidesPerView={1}
        pagination={{ clickable: true }}
      >
        {data.map(({ id, image, name, review }) => {
          return (
            <SwiperSlide key={id} className="testimonials">
              <div className="client__avatar">
                <img src={image} alt="" />
              </div>
              <h5 className="client__name">{name}</h5>
              <small className="client__review">{review}</small>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
};

export default Testimonials;
